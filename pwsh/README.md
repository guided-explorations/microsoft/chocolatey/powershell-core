

[![](https://gitlab.com/guided-explorations/microsoft/chocolatey/powershell-core/badges/main/pipeline.svg)](https://gitlab.com/guided-explorations/microsoft/chocolatey/powershell-core/-/pipelines)
[Update status](https://gitlab.com/guided-explorations/microsoft/chocolatey/powershell-core/-/jobs)
[![](http://transparent-favicon.info/favicon.ico)](#)[chocolatey/DarwinJS](https://chocolatey.org/profiles/DarwinJS)

PowerShell Core is the open source multiplatform version of PowerShell

Preview releases are under the package id powershell-preview so that they can be safely installed and maintained side-by-side with the release versions on production systems. Preview versions do not become the default powershell core edition on a system (unless they are the ONLY edition), if you have a preview version installed, access it via pwsh's '-pre' switch.

This package automatically does verbose MSI logging to %temp%\(packagenameandversion).MsiInstall.log

Some helpful install options (any of them can be combined - delimited by space):

* Any MSI Properties the package responds to can be specified in the same way, even if not documented here.

--install-arguments='"POWERSHELL\_TELEMETRY\_OPTOUT"' -
Valid for PowerShell 7.4 and later - disables built in telemetry.
Read more about PowerShell telemetry here: https://devblogs.microsoft.com/powershell/new-telemetry-in-powershell-7-preview-3/

--install-arguments='"ADD\_FILE\_CONTEXT\_MENU\_RUNPOWERSHELL=1"'
Installs a right click context menu to run .ps1 scripts in PowerShell Core.

--install-arguments='"ADD\_EXPLORER\_CONTEXT\_MENU\_OPENPOWERSHELL=1"'
Installs a right click context menu to start a PowerShell Core prompt for a specific folder.

--install-arguments='"REGISTER\_MANIFEST=1"'
Causes PowerShell Core to deliver logs to Windows Event logs.

--install-arguments='"ENABLE\_PSREMOTING=1"'
Enable PS remoting during installation.

--install-arguments='"USE\_MU=1 ENABLE\_MU=1"'
Enable powershell to be updated via windows updates - this chocolatey package defaults to disabling these. Available with PWSH MSI Package for 7.2 and later.

--install-arguments='"ADD\_EXPLORER\_CONTEXT\_MENU\_OPENPOWERSHELL=1 REGISTER\_MANIFEST=1 ENABLE\_PSREMOTING=1"'
Do it all.

--packageparameters '"/CleanUpPath"'
Removes all powershell core paths before starting install.  Cleans up old paths from old powershell core MSIs.
